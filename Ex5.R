adult_sal <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
str(adult_sal)


#Q2: Reduce the number of levels for country
adult_sal$country

Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")

Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India", "Vietnam")

North.america <- c("United-States", "Canada", "Puerto-Rico") 

Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
Other <- c("South")

inputeCountry <- function(country){
  if(country %in%  Europe){
    return('europe')
  }
  if(country %in%  Asia){
    return('asia')
  }
  if(country %in%  North.america){
    return('north america')
  }
  if(country %in%  Latin.and.south.america){
    return('latin and south america')
  }
  else{
    return ('south')
  }
}

adult_sal$country <- sapply(adult_sal$country, inputeCountry)


summary(adult_sal$marital) #levels= Married-civ-spouse, Never-married, other

inputMarital <- function(marital){
  if(marital=='Married-civ-spouse'){
    return('Married-civ-spouse')
  }
  if(marital=='Never-married'){
    return('Never-married')
  }
  else{
    return('other')
  }
}

adult_sal$marital <- sapply(adult_sal$marital, inputMarital)
str(adult_sal)


summary(adult_sal$type_employer)#levels= Local-gov, Private, Self-emp-inc, Self-emp-not-inc, State-gov, other

inputType_employer <- function(type_employer){
  if(type_employer=='State-gov'){
    return('gov worker')
  }
  if(type_employer=='Federal-gov'){
    return('gov worker')
  }
  if(type_employer=='Local-gov'){
    return('gov worker')
  }
  if(type_employer=='Private'){
    return('Private')
  }
  if(type_employer=='Without-pay'){
    return('Without-pay')
  }
  if(type_employer=='Never-worked'){
    return('Never-worked')
  }
  if(type_employer=='Self-emp-inc'){
    return('Self-emp-inc')
  }
  if(type_employer=='Self-emp-not-inc'){
    return('Self-emp-not-inc')
  } 
  if(type_employer=='?'){
    return(NA)
  }
}
adult_sal$type_employer <- sapply(adult_sal$type_employer, inputType_employer)
str(adult_sal)

adult_sal[adult_sal == '?'] <- NA
adult_sal.clean <- adult_sal[complete.cases(adult_sal),]
any(is.na(adult_sal.clean))
str(adult_sal.clean)


library(dplyr)
adult_sal.clean.noIndex <- select(adult_sal.clean, -X)
str(adult_sal.clean.noIndex)

#Q6 Use histograms to review the effect of the variables on the target attribute  
library(ggplot2)
pl <- ggplot(adult_sal.clean.noIndex, aes(x = hr_per_week, fill=income))
pl<- pl + geom_histogram(binwidth = 20)

pl1 <- ggplot(adult_sal.clean.noIndex, aes(x = age, fill=income))
pl1<- pl1 + geom_histogram(binwidth = 20)

pl2 <- ggplot(adult_sal.clean.noIndex, aes(x = education_num, fill=income))
pl2 <- pl2 + geom_histogram(binwidth = 20)

#Q7 Split the dataset to train and test
#train
adult_sal.train <- sample_frac(adult_sal.clean.noIndex, 0.7)
#num of rows is train set
sid <- as.numeric(rownames(adult_sal.train))
#creating test
adult_sal.test <- adult_sal.clean.noIndex[-sid,]

#Q8 Compute the logistic regression model
#logistic regression
log.model <- glm(income ~ ., family = binomial(link = 'logit'), adult_sal.train)
summary(log.model)

#Q9 Use the test set to find the error rate 
predicted.probabilites <- predict(log.model, adult_sal.test, type = 'response')
predicted.values <- ifelse(predicted.probabilites>0.5,'>50K','<=50K')

#check if the values show the real values (mean error);
misClassError <- mean(predicted.values != adult_sal.test$income)
